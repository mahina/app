'use strict';

angular.module('mahinaApp.marketplaceView', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/marketplaceView', {
    templateUrl: 'marketplaceView/marketplaceView.html',
    controller: 'marketplaceViewCtrl'
  });
}])

.controller('marketplaceViewCtrl', [function() {

}]);