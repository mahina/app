'use strict';

// Declare app level module which depends on views, and components
angular.module('mahinaApp', [
  'ngRoute',
  'mahinaApp.loginView',
  'mahinaApp.marketplaceView',
  'mahinaApp.version',
  'ui.bootstrap'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/loginView'});
}]);
