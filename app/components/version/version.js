'use strict';

angular.module('mahinaApp.version', [
  'mahinaApp.version.interpolate-filter',
  'mahinaApp.version.version-directive'
])

.value('version', '0.1');
