'use strict';

describe('mahinaApp.version module', function() {
  beforeEach(module('mahinaApp.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
