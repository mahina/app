'use strict';

angular.module('mahinaApp.loginView', ['ngRoute', 'ngMessages'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/loginView', {
    templateUrl: 'loginView/login.html',
    controller: 'loginViewCtrl'
  });
}])

.controller('loginViewCtrl', function($scope, $http, $httpParamSerializerJQLike, $window, $location) {
    $scope.username = "";
    $scope.password = "";
    $scope.postError = false;
    $scope.postErrorMessage = "";

    $scope.login = function(valid) {

        $scope.postError = false;
        $http({
            method  : 'POST',
            url     : '/api/authenticate',
            data    : $httpParamSerializerJQLike({
                "username":$scope.username,
                "password":$scope.password
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
        })
        .then(
            function successCallback(data) {
                // TODO: need this location change to be role-based, or perhaps based on last view
                $location.path("/marketplaceView");
            },
            function failureCallback(data) {
                // data.data has error detail if interested
                $scope.postError = true;
                $scope.postErrorMessage = "An error occurred with those credentials."
            }
        );        
    }
});